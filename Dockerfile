# Use the official Node.js image as a base image
FROM node:lts

# Set the working directory
WORKDIR /app/admin

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm ci

# Copy the source code
COPY . .

# Expose the port the app will run on
EXPOSE 3004

# Start the application
CMD ["npm", "start"]
