const { BaseDatabase, BaseResource } = require('adminjs');

class ApiDatabase extends BaseDatabase {
  constructor() {
    super();
  }

  resources() {
    // Return an empty array, as resources are not fetched from the database in this case.
    return [];
  }

  static isAdapterFor(database) {
    return database instanceof ApiDatabase;
  }
}

class ApiResource extends BaseResource {
  constructor(name, endpoint, properties) {
    super(endpoint);
    this._name = name;
    this._properties = properties || {};
  }

  id() {
    return this._name;
  }

  resourceName() {
    return this._name;
  }

  properties() {
    return Object.keys(this._properties).map((key) => this._properties[key]);
  }

  property(path) {
    return this._properties[path];
  }

  static isAdapterFor(resource) {
    return resource instanceof ApiResource;
  }
}

module.exports = { ApiDatabase, ApiResource };
