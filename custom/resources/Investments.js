const { CustomResource } = require('../custom-adapter');

const options = {
  properties: {
    // Define properties for the resource, e.g., for the Investments model
    name: { type: 'string', isTitle: true },
    category: { type: 'reference', target: 'Category', isRequired: true },
    purchaseValue: { type: 'number', isRequired: true },
    value: { type: 'number', isRequired: true },
    currency: { type: 'string', isRequired: true },
    purchaseDate: { type: 'date', isRequired: true },
    additionalInfo: { type: 'mixed' },
  },
};

const investmentsResource = new CustomResource(
  process.env.INVESTMENTS_API_URL,
  'Investments',
  { ...options }
);

module.exports = () => investmentsResource;
