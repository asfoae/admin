// const { CustomResource } = require('../custom-adapter')

// const options = {
//   properties: {
//     // Define properties for the resource, e.g., for the Asset model
//     name: { type: 'string', isTitle: true },
//     category: { type: 'reference', target: 'Category', isRequired: true },
//     value: { type: 'number', isRequired: true },
//     purchaseValue: { type: 'number', isRequired: true },
//     location: { type: 'string' },
//     acquisitionDate: { type: 'date' },
//     additionalInfo: { type: 'mixed' },
//   },
// };

// const assetResource = new CustomResource(
//   process.env.ASSETMANAGEMENT_API_URL,
//   'Asset',
//   { ...options }
// );

// module.exports = () => assetResource;

module.exports = {
  // The resource name
  name: 'Asset',

  // The resource endpoint
  endpoint: 'http://localhost:3000',

  // The resource properties
  properties: {
    // Add your properties here
    name: { type: 'string', isTitle: true },
    category: { type: 'reference', target: 'Category', isRequired: true },
    value: { type: 'number', isRequired: true },
    purchaseValue: { type: 'number', isRequired: true },
    location: { type: 'string' },
    acquisitionDate: { type: 'date' },
    additionalInfo: { type: 'mixed' },
  },
};
