const { CustomResource } = require('../custom-adapter');

const options = {
  properties: {
    // Define properties for the resource, e.g., for the Debts model
    creditor: { type: 'string', isTitle: true },
    type: { type: 'string', isRequired: true },
    initialBalance: { type: 'number', isRequired: true },
    balanceLeft: { type: 'number', isRequired: true },
    currency: { type: 'string', isRequired: true },
    interestRate: { type: 'number', isRequired: true },
    startDate: { type: 'date', isRequired: true },
    'duration.value': { type: 'number', isRequired: true },
    'duration.unit': { type: 'string', isRequired: true, availableValues: ['week', 'month', 'year'] },
    'paymentFrequency.value': { type: 'number', isRequired: true },
    'paymentFrequency.unit': { type: 'string', isRequired: true, availableValues: ['week', 'month', 'year'] },
    isRevolving: { type: 'boolean' },
  },
};

const debtsResource = new CustomResource(
  process.env.DEBTS_API_URL,
  'Debts',
  { ...options }
 );

module.exports = () => debtsResource;