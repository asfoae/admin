const axios = require('axios');
const { BaseDatabase, BaseResource, BaseRecord } = require('adminjs');

class CustomDatabase extends BaseDatabase {
  // Implement the required methods for the Database component
}

class CustomResource extends BaseResource {
  constructor(endpoint, resource, options = {}) {
    super({ ...resource, id: resource.name });
    this._properties = options.properties;
    this._resource = resource;
    this._endpoint = endpoint;
  }

  // Implement required methods for AdminJS

  async findOne(id) {
    try {
      const response = await axios.get(`${this._endpoint}/${id}`);
      return new CustomRecord(response.data, this);
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  async find(filter, { limit = 10, offset = 0, sort = {} }) {
    try {
      const response = await axios.get(this._endpoint, {
        params: {
          filter,
          limit,
          offset,
          sort,
        },
      });
      return {
        records: response.data.map(record => new CustomRecord(record, this)),
        meta: {
          total: response.data.length,
          limit,
          offset,
        },
      };
    } catch (error) {
      console.error(error);
      return { records: [], meta: { total: 0, limit, offset } };
    }
  }

  async count(filter) {
    try {
      const response = await axios.get(`${this._endpoint}/count`, {
        params: { filter },
      });
      return response.data.count;
    } catch (error) {
      console.error(error);
      return 0;
    }
  }

  async create(params) {
    try {
      const response = await axios.post(this._endpoint, params);
      return new CustomRecord(response.data, this);
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  async update(id, params) {
    try {
      const response = await axios.put(`${this._endpoint}/${id}`, params);
      return new CustomRecord(response.data, this);
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  async delete(id) {
    try {
      await axios.delete(`${this._endpoint}/${id}`);
      return true;
    } catch (error) {
      console.error(error);
      return false;
    }
  }

  static isAdapterFor(resource) {
    // Return true as this custom adapter is built for your resources
    return true;
  }

  properties() {
    console.log('Properties:', this._properties);
    return Object.keys(this._properties).map(
      (name) => new Property({ ...this._properties[name], path: name })
    );
  }

  id() {
    return this._resource;
  }
  
}

class CustomRecord extends BaseRecord {
  constructor(params, resource) {
    super(params, resource);
  }

  // Implement required methods for AdminJS

  id() {
    return this.params._id || this.params.id || this.params.Id || this.params.ID;
  }
}


module.exports = {
  CustomResource,
  CustomRecord,
  CustomDatabase,
};
