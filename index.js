const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const connectDB = require('./config/database');

const formidableMiddleware = require('express-formidable');
const session = require('express-session');
const AdminBro = require('adminjs');
const AdminBroExpress = require('@adminjs/express');

const { ApiDatabase, ApiResource } = require('./custom/api-adapter');

const assetResource = require('./custom/resources/Asset');
const debtsResource = require('./custom/resources/Debts');
const investmentsResource = require('./custom/resources/Investments');

const Keycloak = require('keycloak-connect');

// Create a session store for Keycloak
const memoryStore = new session.MemoryStore();

// Configure Keycloak middleware
const keycloakConfig = require('./config/keycloak.json');
const keycloak = new Keycloak({ store: memoryStore }, keycloakConfig);

// Connect to MongoDB
//connectDB();

const app = express();

// AdminJS part
AdminBro.registerAdapter({ Database: ApiDatabase, Resource: ApiResource });

const adminBro = new AdminBro({
  databases: [new ApiDatabase()],
  resources: [
    new ApiResource(assetResource.name, assetResource.endpoint, assetResource.properties),
    // new ApiResource(debtsResource.name, debtsResource.endpoint, debtsResource.properties),
    // new ApiResource(investmentsResource.name, investmentsResource.endpoint, investmentsResource.properties),
  ],
  rootPath: '/admin',
});

app.use(adminBro.options.rootPath, session({
  secret: 'your-session-secret',
  resave: false,
  saveUninitialized: true,
  key: 'keycloak-token',
  store: memoryStore,
}));

// Use the Keycloak middleware
app.use(keycloak.middleware({ admin: adminBro.options.rootPath }));

app.use(formidableMiddleware());

app.use(adminBro.options.rootPath, AdminBroExpress.buildAuthenticatedRouter(adminBro, {
  authenticate: async (email, password) => {
    // Use Keycloak to authenticate the user
    const grant = await keycloak.grantManager.obtainDirectly(email, password);
    if (grant) {
      return {
        email,
        accessToken: grant.access_token.token,
      };
    }
    return false;
  },
  cookieName: 'adminbruh',
  cookiePassword: 'your-cookie-password',
}));

// Middleware
app.use(bodyParser.json());
app.use(cors());
app.use(helmet());

// Routes
app.get('/', (req, res) => {
  res.json({ message: 'Admin API' });
});

const categoriesRouter = require('./routes/categories');

app.use('/categories', categoriesRouter);

// Start the server
const PORT = process.env.PORT || 3004;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
