const express = require('express');
const router = express.Router();

const Category = require('../models/Category');

// Get all categories
router.get('/', async (req, res) => {
  try {
    const categories = await Category.find();
    res.json(categories);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Create a new category
router.post('/', async (req, res) => {
  const category = new Category(req.body);

  try {
    const newCategory = await category.save();
    res.status(201).json(newCategory);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Get a specific category by ID
router.get('/:id', getCategoryById, (req, res) => {
  res.json(res.category);
});

// Update a specific category by ID
router.put('/:id', getCategoryById, async (req, res) => {
  try {
    Object.assign(res.category, req.body);
    const updatedCategory = await res.category.save();
    res.json(updatedCategory);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Delete a specific category by ID
router.delete('/:id', getCategoryById, async (req, res) => {
  try {
    await res.category.remove();
    res.json({ message: 'Category deleted' });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Middleware to find a category by ID
async function getCategoryById(req, res, next) {
  let category;
  try {
    category = await Category.findById(req.params.id);
    if (!category) {
      return res.status(404).json({ message: 'Category not found' });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }

  res.category = category;
  next();
}

module.exports = router;
